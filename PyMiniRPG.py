# LICENSE General Public License Version 3
# However please give credit to the creator of the original code 
# Made by Vlad C.
# Provided on AS IS basis without any warranty of any kind, either expressed or implied
# Use it at your own risk
# Pyminirpg 1.0.2
# Do not remove the header from this file or other files created from it
import random

npc={
	'hp':0,
	'arm':0,
	'attk':0,
	'mp':0
	}#lvl are as buffs

player={
	'hp':random.randrange(10),
	'arm':random.randrange(3),
	'attk':random.randrange(10),
	'mp':random.randrange(5),
	'exp':0,
	'lvl':0
	}
def place(lvl):
	selectable=[]
	places=['village ', 'forest ', 'house ', 'well ', 'mill ']
	count=0
	for place in places:
		i=random.randrange(3)
		if count==0: i=0
		if i==0:
			count+=1
			selectable.append(place)
	selectable=''.join(selectable)
	return selectable

def pres_npc(dest,lvl):
	npcs=['clown ', 'beggar ', 'bear ', 'shaman ', 'mage ', 'smith ', 'carpenter ']
	baddies=['goblin ', 'scholar ', 'necromancer ', 'evil clown ', 'zombie ', 'squirrel ', 'rat ', 'monkey ']
	boss=['cloned clown ','undead shoemaker ','Dorth the heroic ']
	out_npc=[]

	if dest=='village':
		for n in npcs:
			i=random.randrange(1)
			if i==0:
				out_npc.append(n)
	if dest=='forest':
		for n in baddies:
			i=random.randrange(4)
			if i>=1:
				out_npc.append(n)
	if dest=='house':
		count=0
		for n in npcs:
			i=random.randrange(3)
			if count<1:
				i=1
			if i==1:
				out_npc.append(n)
	if dest=='well':
		for n in baddies:
			i=random.randrange(3)
			if i==0:
				out_npc.append(n)
	if dest=='mill':
		for n in baddies:
			i=random.randrange(3)
			if i==0:
				out_npc.append(n)
		for n in 'boss':
			i=random.randrange(12)
			if i==1:
				out_npc.append(n)
		out_npc=''.join(out_npc)
	return out_npc

print('Welcome to mini rpg (1.0.2), Enjoy your stay!')
name=raw_input('Your name sir: ')
print('%s, Your stats are'%(name),player)
alive=True
destination=''
while alive==True:
	if player['exp']>=100:
		player['lvl']+=1
		player['exp']-=100
		i=random.randrange(4)
		if i==0: 
			stat_awarded='hp'
			player['hp']+=random.randrange(player['lvl']+2)
		if i==1: 
			stat_awarded='arm'
			player['arm']+=random.randrange(player['lvl'])
		if i==2: 
			stat_awarded='attk'
			player['attk']+=random.randrange(player['lvl']+1)
		if i==3: 
			stat_awarded='mp'
			player['mp']+=random.randrange(player['lvl']+1)			
		print ('LVL UP! Stats are:',player)

	feed_place=place(player['lvl'])
	print feed_place
	destination=raw_input('Where would you like to go next? ')
	if destination=='quit': break
	npcs_event=pres_npc(destination,player['lvl'])
	print ('You see: ', npcs_event, 'What is your action?(type in npc)')
	action=raw_input()
	if action=='quit': 
		break
	interact=raw_input('Do you want to attack or trade (baddies don\'t trade)? ')
	if action=='quit' or interact=='quit':break
	if interact=='attack':
		enemy_stats=npc
		enemy_stats['hp']=player['lvl']+random.randrange(1,player['lvl']/2+5)
		enemy_stats['arm']=random.randrange(3+abs(player['lvl']-3))
		enemy_stats['attk']=random.randrange(1, abs(player['lvl']-3)+1)
		enemy_stats['mp']=random.randrange(5)
		print ('enemy stats are: ',enemy_stats)
		while enemy_stats['hp']<1 or player['hp']<1:
			print('enemy hp: ', enemy_stats['hp'])
			attack=raw_input('melee or magic attack? ')
			if attack=='quit':
				alive=False
				player['hp']=0
			if attack=='melee':
				enemy_stats['hp']-=abs(random.randrange(player['attk']+1)-enemy_stats['arm'])
			elif attack=='magic':
				enemy_stats['hp']-=abs(random.randrange(player['mp']+1)-enemy_stats['arm'])
			if enemy_stats['attk']>enemy_stats['mp']:
				player['hp']-=abs(random.randrange(enemy_stats['attk']+1)-player['arm'])
			else:
				player['hp']-=abs(random.randrange(enemy_stats['mp']+1)-player['arm'])
		else:
			if player['hp']<1:
				print ('game over')
				break
			elif enemy_stats['hp']<1:
				ex=random.randrange(20)
				print ('Awarded %d experience' %(ex))
				player['exp']+=ex

	if interact=='trade':
		i=random.randrange(10)
		if i==0:
			i=random.randrange(4)
			if i==0: 
				stat_awarded='hp'
				player['hp']+=1
			if i==1: 
				stat_awarded='arm'
				player['arm']+=1
			if i==2: 
				stat_awarded='attk'
				player['attk']+=1
			if i==3: 
				stat_awarded='mp'
				player['mp']+=1
			if i==4: 
				stat_awarded="lvl"
				player['lvl']+=1
			print('You got +1 stat %s' %(stat_awarded))
		else:
			print('%s walks away...'%(action))